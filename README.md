# Duck in Town

Homepage: [https://papaya-games.itch.io/duck-in-town](https://papaya-games.itch.io/duck-in-town)

## Index

- [About](#about)
- [Features](#features)
- [Download](#download)
- [License](#license)

## About

Duck in Town is a multiepisodic 3D graphic adventure heavily inspired by 
the classics of the genre.  

Made with [Godot Engine](https://godotengine.org).

You can see more info (including the devlog) [here](https://papaya-games.itch.io/duck-in-town).

## Features

- Discover the mysteries of Town, capital of the Duck Kingdom
- Meet the Duck Knights and help Duckson become one of them
- Play as a cute duck
- Interact with the quirky Town villagers
- Win the squawk contest
- Did I mention ducks?


## Download

- [Steam](https://store.steampowered.com/app/1111310/Duck_in_Town__A_Rising_Knight/)
- [itch.io](https://papaya-games.itch.io/duck-in-town)

## License

This repository contains the source code for the game, which is licensed under
the GPLv3. However, it doesn't contain all necesary assets to play the game,
since these are not open. To play the game, get an official release binary.
Check the [LICENSE file](LICENSE) for more details.

