extends KinematicBody

const GRAVITY = -9.8
const WALK_SPEED = 3.0
const SPRINT_SPEED = 8.0
const ACCEL = 7
const DEACCEL = 15

# Animations
const IDLE_ANIM := "idle-loop"
const IDLE_ACTIONS := ["IdleactionOne-loop", "IdleactionTwo"]
const TALK_ANIM := "Talk-loop"
const WALK_ANIM := "Walk-loop"
const RUN_ANIM := "Run-loop"
const SQUAWK_ANIM := "Squawk-loop"
const GRAB_ANIM := "grab"

onready var anim_player := $Body/Model/AnimationPlayer as AnimationPlayer
onready var interact_area := $InteractArea as Area

var current_cam_transform := Transform()
var vel := Vector3()
var dir := Vector3()

var _current_actions := []

var is_talking := false
var is_on_dialog := false
var is_moving := false
var is_squawking := false # set to true when playing the squawking minigame
var is_grabbing := false

signal can_change_cam_transform
signal grab_anim_finished


var previous_input_dir := Vector2()

func _ready() -> void:
	GlobalStatus.player = self
	anim_player.play(IDLE_ANIM)
	GUI.connect("dialog_started", self, "_on_dialog_started")
	# Reset actions being shown.
	# This is done because there is one Duckson for each scene
	GUI.set_actions([])
	# Test items
	#GlobalStatus.owned_items = ["barrel", "box", "coindarling", "nachos", "hanapicture"]


func _unhandled_input(event: InputEvent) -> void:
	if not is_on_dialog and not is_squawking:
		if event.is_action_pressed("pause") and not GUI.pause_menu.visible:
			var img = get_viewport().get_texture().get_data()
			img.flip_y()
			GUI.pause_menu.save_data_screenshot = img
			GUI.pause_menu.show()
		elif event.is_action_pressed("show_inventory"):
			GUI.select_item()


func _physics_process(delta: float) -> void:
	if current_cam_transform == Transform():
		return
	
	# Dialog anim handling
	if is_squawking:
		anim_player.play(SQUAWK_ANIM, -1, 2)
		return
	elif is_talking:
		anim_player.play(TALK_ANIM)
		return
	if is_grabbing:
		anim_player.play(GRAB_ANIM, -1, 2)
		return
	elif is_on_dialog:
		anim_player.play(IDLE_ANIM)
		$IdleAnimTimer.paused = true
		return
	else:
		# If timer was paused, restart it
		if $IdleAnimTimer.paused:
			$IdleAnimTimer.paused = false
			$IdleAnimTimer.start()
	
	_process_interaction()

	# Movement input
	dir = Vector3()
	var input_dir := Vector2()

	input_dir.y = Input.get_action_strength("move_forward") - Input.get_action_strength("move_backward")
	input_dir.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	input_dir = input_dir.normalized()
	
	if previous_input_dir != Vector2():
		var input_angle = rad2deg(input_dir.angle_to(previous_input_dir))
		if abs(input_angle) > 10:
			# If input dir changes enough, change cam transform if needed
			emit_signal("can_change_cam_transform")
	
	previous_input_dir = input_dir
	
	# set dir
	dir += -current_cam_transform.basis.z.normalized() * input_dir.y
	dir += current_cam_transform.basis.x.normalized() * input_dir.x
	
	if is_moving and dir.length() <= 0:
		# stopped moving, can change cam transform
		emit_signal("can_change_cam_transform")
	is_moving = dir.length() > 0

	# Process movement
	dir.y = 0

	vel.y += GRAVITY * delta

	var hvel := vel
	hvel.y = 0
	
	var is_sprinting := Input.is_action_pressed("sprint")

	var target := dir * (SPRINT_SPEED if is_sprinting else WALK_SPEED)
	# choose acceleration
	var accel : float
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	# interpolate velocity and move the body
	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel, Vector3(0, 1, 0), true)

	var anim_to_play = null
	
	# If playing idle action, keep it
	for idle_action_anim in IDLE_ACTIONS:
		if anim_player.current_animation == idle_action_anim:
			anim_to_play = idle_action_anim
	
	if anim_to_play == null:
		anim_to_play = IDLE_ANIM
	
	# facing
	if dir.length() > 0:
		anim_to_play = RUN_ANIM if is_sprinting else WALK_ANIM
		var angle : float = atan2(hvel.x, hvel.z)
		self.rotation_degrees.y = rad2deg(angle)
	if anim_to_play != anim_player.current_animation:
		var custom_speed := 1.0
		match anim_to_play:
			RUN_ANIM:
				custom_speed = 1.5
			WALK_ANIM:
				custom_speed = 3.5
			IDLE_ANIM:
				$IdleAnimTimer.start()
		anim_player.play(anim_to_play, -1, custom_speed)


func _process_interaction() -> void:
	var areas := interact_area.get_overlapping_areas()
	var objects := []
	for a in areas:
		# Allow InteractiveObject script to be applied
		# to the mesh or the area
		if a is InteractiveObject:
			objects.append(a)
		elif a.get_parent() is InteractiveObject:
			objects.append(a.get_parent())
	
	var actions := []
	# Action == [VERB, TARGET]
	for o in objects:
		if o.is_portal:
			actions.append([ActionVerbs.GO_TO, o])
			continue
		if o.can_be_talked_looked:
			actions.append([ActionVerbs.TALK_LOOK, o])
		if o.can_receive_object:
			actions.append([ActionVerbs.GIVE_USE_OBJECT, o])
		if o.is_takeable:
			actions.append([ActionVerbs.TAKE, o])

	
	if actions != _current_actions:
		_current_actions = actions
		GUI.set_actions(_current_actions)


func change_to_cam(c : Camera, custom_transform : Spatial = null) -> void:
	c.current = true
	if current_cam_transform != Transform():
		# only wait to movement stopped if this isn't 
		# the initial camera setup. Waiting for this 
		# results in an smoother movement between camera
		# angles (transform only changes when movement is stopped)
		yield(self, "can_change_cam_transform")
	if custom_transform != null:
		current_cam_transform = custom_transform.global_transform
	else:
		current_cam_transform = c.global_transform


func play_grab_anim() -> void:
	# custom anim speed
	is_grabbing = true
	anim_player.play(GRAB_ANIM, -1, 2)
	yield(anim_player, "animation_finished")
	emit_signal("grab_anim_finished")
	is_grabbing = false


func _on_dialog_finished() -> void:
	is_on_dialog = false
	is_talking = false


func _on_dialog_started() -> void:
	is_on_dialog = true
	is_talking = false


func _current_dialog_char_name_changed(char_name : String) -> void:
	is_talking = char_name == "Duckson"


func _on_IdleAnimTimer_timeout() -> void:
	if not is_squawking:
		anim_player.play(IDLE_ACTIONS[randi() % IDLE_ACTIONS.size()])


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "IdleactionTwo":
		anim_player.play(IDLE_ANIM)
		$IdleAnimTimer.start()
