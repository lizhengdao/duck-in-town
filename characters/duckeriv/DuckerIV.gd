extends InteractiveObject

func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Ducker IV"
	._ready()


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["duckeriv_coinroyal_given"]):
		dialog_id = "duckeriv_duckson_postgame_3"
	elif GlobalStatus.story_flags_unlocked(["coinlilly_given"]):
		dialog_id = "duckeriv_duckson_postgame_2"
	elif GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "duckeriv_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["duckeriv_trophy_given"]):
		dialog_id = "duckeriv_duckson_5"
	elif GlobalStatus.story_flags_unlocked(["duckeriv_duckson_3_finished"]):
		dialog_id = "duckeriv_duckson_4"
	elif GlobalStatus.story_flags_unlocked(["squawk_contest_won"]):
		dialog_id = "duckeriv_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["duckeriv_duckson_1_finished"]):
		dialog_id = "duckeriv_duckson_2"
	else:
		dialog_id = "duckeriv_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"coinduckeriv":
			dialog_id = "duckeriv_give_coinduckeriv"
		"trophy":
			if GlobalStatus.story_flags_unlocked(["postgame_started"]):
				dialog_id = "duckeriv_give_trophy_postgame"
			else:
				dialog_id = "duckeriv_give_trophy"
		"coinroyal":
			if GlobalStatus.story_flags_unlocked(["postgame_started"]):
				dialog_id = "duckeriv_give_coinroyal_postgame"
			else:
				dialog_id = "duckeriv_give_anything"
		_:
			dialog_id = "duckeriv_give_anything"
	GUI.start_dialog(dialog_id, [self])