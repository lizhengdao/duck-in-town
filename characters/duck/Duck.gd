extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Duck"
	._ready()


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["durkson_quest"]):
		dialog_id = "duck_duckson_postgame_2"
	elif GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "duck_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["duck_duckson_1_finished", "forest_post_read"]):
		dialog_id = "duck_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["duck_duckson_1_finished"]):
		dialog_id = "duck_duckson_2"
	else:
		dialog_id = "duck_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"nachos":
			dialog_id = "duck_give_nachos"
		"trophy":
			dialog_id = "duck_give_trophy"
		_:
			dialog_id = "duck_give_anything"
	GUI.start_dialog(dialog_id, [self])