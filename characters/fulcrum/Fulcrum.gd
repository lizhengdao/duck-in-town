extends InteractiveObject


func _ready() -> void:
	is_npc = true
	is_takeable = false
	object_name = "Fulcrum"
	._ready()


func talk_look() -> void:
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["coinlilly_given"]):
		dialog_id = "fulcrum_duckson_postgame_3"
	elif GlobalStatus.story_flags_unlocked(["durkson_duckson_postgame_3_finished"]):
		dialog_id = "fulcrum_duckson_postgame_2"
	elif GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "fulcrum_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["fulcrum_duckson_1_finished"]):
		dialog_id = "fulcrum_duckson_2"
	else:
		dialog_id = "fulcrum_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"trophy":
			dialog_id = "fulcrum_give_trophy"
		"box":
			dialog_id = "fulcrum_give_box"
		"nachos":
			dialog_id = "fulcrum_give_nachos"
		"coinroyal":
			dialog_id = "fulcrum_give_coinroyal"
		"gatopicture":
			dialog_id = "fulcrum_give_gatopicture"
			GlobalStatus.add_story_flag("gato_paint_given_to_fulcrum")
		"hanapicture":
			dialog_id = "fulcrum_give_hanapicture"
			GlobalStatus.add_story_flag("hana_paint_given_to_fulcrum")
		"coindogge":
			dialog_id = "fulcrum_give_coindogge"
		"coinfulcrum":
			if GlobalStatus.story_flags_unlocked(["coinfulcrum_changed"]):
				dialog_id = "fulcrum_give_coinfulcrum_2"
			else:
				dialog_id = "fulcrum_give_coinfulcrum"
		"coinlilly":
			if GlobalStatus.story_flags_unlocked(["postgame_started"]):
				dialog_id = "fulcrum_give_coinlilly_postgame"
			else:
				dialog_id = "fulcrum_give_anycoin"
		"coindarling", "coinduckeriv", "coindurkson":
			dialog_id = "fulcrum_give_anycoin"
		_:
			dialog_id = "fulcrum_give_anything"
	GUI.start_dialog(dialog_id, [self])