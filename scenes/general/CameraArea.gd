extends Area
class_name CameraArea

export(NodePath) var camera_path = null
# If this transform is set, it will define
# player movement
export(NodePath) var custom_transform = null

# variable needed to make sure there is no area being overriden by another one
# see on_body_exited comment for more info
var just_exited := false

func _ready() -> void:
#warning-ignore:return_value_discarded
	self.connect("body_entered", self, "on_body_entered")
	self.connect("body_exited", self, "on_body_exited")
	self.add_to_group("CameraAreas")
	# We set this manually because there is a problem with FOV importer
	# from Godot. Doing it from here instead of the editor
	# allows us to reimport more easily. See this issue for more info:
	# https://github.com/godotengine/godot-blender-exporter/issues/109
	(get_node(camera_path) as Camera).fov = 29
	(get_node(camera_path) as Camera).far = 200


func on_body_entered(body : Node) -> void:
	if body != GlobalStatus.player:
		return
	
	just_exited = false
	var cam := get_node(camera_path) as Camera
	cam.fov = 29
	cam.far = 200
	if custom_transform != null:
		GlobalStatus.player.change_to_cam(cam, get_node(custom_transform) as Spatial)
	else:
		GlobalStatus.player.change_to_cam(cam)


func on_body_exited(body : Node) -> void:
	# Notify other areas that the body has exited from this one.
	# This is done to make sure the player standing between two areas
	# doesn't break camera switching. If the player stands between two areas,
	# when it moves and exits from one of them, the other one will be notified and enabled
	if body == GlobalStatus.player:
		just_exited = true
		for area in get_tree().get_nodes_in_group("CameraAreas"):
			area.on_some_camera_area_exited()


func on_some_camera_area_exited() -> void:
	var bs = get_overlapping_bodies()
	if GlobalStatus.player in bs and not just_exited:
		on_body_entered(GlobalStatus.player)