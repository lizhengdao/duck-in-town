extends Label

signal minigame_started

# These funcs are called on the countdown anim

func play_countdown_sound() -> void:
	GlobalStatus.play_sound("res://assets/minigames/squawk/countdown.ogg")


func emit_minigame_started_signal() -> void:
	emit_signal("minigame_started")
