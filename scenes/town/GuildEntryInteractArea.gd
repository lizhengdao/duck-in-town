extends InteractiveObject

func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = true
	object_name = tr("GUILD_NAME_LABEL")


func go_to() -> void:
	GlobalStatus.change_scene_to("res://scenes/guild/Guild.tscn")
