extends Area


func _on_InfiniteEndArea_body_entered(body: Node) -> void:
	if body != GlobalStatus.player:
		return
	
	GlobalStatus.player.global_transform.origin.z -= 58
