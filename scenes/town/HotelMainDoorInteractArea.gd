extends InteractiveObject


func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = false
	object_name = tr("HOTEL_DOOR_NAME_LABEL")


func talk_look():
	.talk_look()
	GUI.start_dialog("hotel_main_door", [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	GUI.start_dialog("hotel_main_door_give_anything", [self])