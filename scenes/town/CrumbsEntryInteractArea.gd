extends InteractiveObject

func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = true
	object_name = "Crumb's"


func go_to() -> void:
	GlobalStatus.change_scene_to("res://scenes/crumbs/Crumbs.tscn")