extends InteractiveObject


func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = false
	object_name = tr("YELLOW_FAIR_STAND_NAME_LABEL")


func talk_look():
	.talk_look()
	GUI.start_dialog("yellow_fair_stand", [self])