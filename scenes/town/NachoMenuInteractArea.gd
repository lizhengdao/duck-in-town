extends InteractiveObject


func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = false
	object_name = tr("EL_NACHO_QUESOSO_MENU_NAME_LABEL")


func talk_look():
	.talk_look()
	GUI.start_dialog("nacho_menu", [self])