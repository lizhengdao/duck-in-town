extends InteractiveObject

func _ready() -> void:
	is_npc = true
	npc_turn_on_talk = false
	is_takeable = false
	is_portal = false
	object_name = "Mike"


func talk_look():
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["durkson_quest", "duck_durkson_quest"]):
		dialog_id = "mike_duckson_postgame_2"
	elif GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "mike_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["anvy_read", "durkson_favor"]):
		dialog_id = "mike_duckson_4"
	elif GlobalStatus.story_flags_unlocked(["durkson_favor"]):
		dialog_id = "mike_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["anvy_read"]):
		dialog_id = "mike_duckson_2"
	else:
		dialog_id = "mike_duckson_1"
	GUI.start_dialog(dialog_id, [self])


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"coinroyal":
			dialog_id = "mike_give_coinroyal"
		"coindarling", "coindogge", "coinduckeriv", "coindurkson", "coinfulcrum", "coinlilly":
			dialog_id = "mike_give_anycoin"
		"trophy":
			if GlobalStatus.story_flags_unlocked(["postgame_started"]):
				GUI.start_dialog("mike_give_trophy_postgame", [self])
				yield(GUI, "dialog_finished")
				GlobalStatus.change_scene_to("res://scenes/theend/TheEnd.tscn")
				return
			else:
				dialog_id = "mike_give_anything"
		_:
			dialog_id = "mike_give_anything"
	GUI.start_dialog(dialog_id, [self])