extends InteractiveObject

func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = true
	object_name = "arcade" if GlobalStatus.story_flags_unlocked(["arcade_open"]) else tr("WAREHOUSE_NAME_LABEL")


func go_to() -> void:
	GlobalStatus.change_scene_to("res://scenes/crumbs_arcade/CrumbsArcade.tscn")