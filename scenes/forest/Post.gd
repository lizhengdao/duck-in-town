extends InteractiveObject


func _ready() -> void:
	is_npc = false
	is_takeable = false
	object_name = tr("POST_NAME_LABEL")


func talk_look() -> void:
	.talk_look()
	GUI.start_dialog("post_forest", [self])
