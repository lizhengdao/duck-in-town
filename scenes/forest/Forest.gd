extends Spatial

export(AudioStream) var forest_music = null


func _ready() -> void:
	if forest_music == null:
		printerr("Forest music null at Forest.tscn")
	else:
		GlobalStatus.play_scene_music(forest_music.resource_path, -13)
	
	if not GlobalStatus.story_flags_unlocked(["duck_duckson_1_finished"]):
		GlobalStatus.player.global_transform = Transform(\
			Vector3(1, 0, 0),
			Vector3(0, 1, 0),
			Vector3(0, 0, 1),
			Vector3(-0.2, 0, -2.978)
		)
		$Duck.talk_look()
	
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		$Duck.hide()
		$Duck/InteractArea.monitorable = false
		$Duck/InteractArea.monitoring = false
		$Duck/StaticBody/CollisionShape.disabled = true