extends InteractiveObject

onready var pato_node : Spatial = $"../Pato" as Spatial
onready var dogge_node : Spatial = $"../Dogge" as Spatial

func _ready() -> void:
	is_npc = true
	is_takeable = false
	is_portal = false
	object_name = tr("DOGGE_AND_PATO_LABEL")


func talk_look() -> void:
	.talk_look()
	pato_node.talk_look()
	dogge_node.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["arnold_quest_solved"]):
		dialog_id = "pato_dogge_duckson_postgame_3"
	elif GlobalStatus.story_flags_unlocked(["arnold_quest"]):
		dialog_id = "pato_dogge_duckson_postgame_2"
	elif GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "pato_dogge_duckson_postgame_1"
	elif GlobalStatus.story_flags_unlocked(["pato_dogge_duckson_5_finished", "hana_paint_received_from_pato"]) or GlobalStatus.story_flags_unlocked(["pato_dogge_duckson_6_finished", "hana_paint_received_from_pato"]):
		dialog_id = "pato_dogge_duckson_8"
	elif GlobalStatus.story_flags_unlocked(["pato_dogge_duckson_5_finished", "gato_paint_given_to_fulcrum"]) or GlobalStatus.story_flags_unlocked(["pato_dogge_duckson_6_finished", "gato_paint_given_to_fulcrum"]):
		dialog_id = "pato_dogge_duckson_7"
	elif GlobalStatus.story_flags_unlocked(["pato_dogge_duckson_4_finished", "gato_paint_given_to_fulcrum"]):
		dialog_id = "pato_dogge_duckson_6"
	elif GlobalStatus.story_flags_unlocked(["pato_dogge_duckson_3_finished", "gato_paint_given_to_fulcrum"]):
		dialog_id = "pato_dogge_duckson_5"
	elif GlobalStatus.story_flags_unlocked(["duckeriii_duckson_1_finished", "pato_dogge_duckson_1_finished"]):
		dialog_id = "pato_dogge_duckson_3"
	elif GlobalStatus.story_flags_unlocked(["duckeriii_duckson_1_finished"]):
		dialog_id = "pato_dogge_duckson_4"
	elif GlobalStatus.story_flags_unlocked(["pato_dogge_duckson_1_finished"]):
		dialog_id = "pato_dogge_duckson_2"
	else:
		dialog_id = "pato_dogge_duckson_1"
	GUI.start_dialog(dialog_id, [self, pato_node, dogge_node]) # we add the area itself so it's notified when the dialog ends
	yield(GUI, "dialog_finished")
	if dialog_id in ["pato_dogge_duckson_7", "pato_dogge_duckson_6", "pato_dogge_duckson_5"]:
		get_parent().hide_hana_picture()


func give_use_object(_obj_id : String) -> void:
	.talk_look()
	var dialog_id := ""
	match _obj_id:
		"barrel":
			dialog_id = "pato_dogge_give_barrel"
		"box":
			dialog_id = "pato_dogge_give_box"
		"coindarling", "coinduckeriv", "coindurkson", "coinlilly", "coinfulcrum", "coindogge":
			dialog_id = "pato_dogge_give_anycoin"
		"coinroyal":
			dialog_id = "pato_dogge_give_coinroyal"
		"gatopicture":
			dialog_id = "pato_dogge_give_gatopicture"
		"hanapicture":
			dialog_id = "pato_dogge_give_hanapicture"
		"nachos":
			dialog_id = "pato_dogge_give_nachos"
		"royalcoinonastring":
			dialog_id = "pato_dogge_give_royalcoinonastring"
		"trophy":
			dialog_id = "pato_dogge_give_trophy"
	GUI.start_dialog(dialog_id, [self, pato_node, dogge_node])