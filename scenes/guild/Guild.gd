extends Spatial

export(AudioStream) var guild_music = null

func _ready() -> void:
	if guild_music == null:
		printerr("Guild music null at Guild.tscn")
	else:
		GlobalStatus.play_scene_music(guild_music.resource_path, -10)
	
	if GlobalStatus.story_flags_unlocked(["hana_paint_received_from_pato"]):
		$Model/HanaPicture.hide()
	else:
		$Model/HanaPicture.show()
	
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		$Model/GatoPicture.show()
	else:
		$Model/GatoPicture.hide()


func hide_hana_picture() -> void:
	$Model/HanaPicture.hide()