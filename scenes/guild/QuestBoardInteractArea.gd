extends InteractiveObject


func _ready() -> void:
	is_npc = false
	is_takeable = false
	is_portal = false
	can_receive_object = false
	object_name = tr("QUEST_BOARD_NAME_LABEL")


func talk_look():
	.talk_look()
	var dialog_id := ""
	if GlobalStatus.story_flags_unlocked(["postgame_started"]):
		dialog_id = "quest_board_2"
	else:
		dialog_id = "quest_board"
	GUI.start_dialog(dialog_id, [self])