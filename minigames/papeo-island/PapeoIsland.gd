extends Node

# This scene handles PapeoIsland's scene changes without messing 
# with the SceneTree

export(PackedScene) var main_menu_scn = null
export(PackedScene) var game_scn = null

var _main_menu = null
var _game = null

func _ready() -> void:
	if main_menu_scn == null:
		printerr("MainMenu.tscn is null at PapeoIsland.tscn")
	if game_scn == null:
		printerr("Game.scn is null at PapeoIsland.tscn")
	
	yield($Splash, "splash_ended")
	$Splash.queue_free()
	_on_game_over() # load main menu


func _on_game_started() -> void:
	_game = game_scn.instance()
	if _main_menu != null and is_instance_valid(_main_menu):
		_main_menu.queue_free()
	_main_menu = null
	add_child(_game)
	_game.connect("game_over", self, "_on_game_over")


func _on_game_over() -> void:
	_main_menu = main_menu_scn.instance()
	if _game != null and is_instance_valid(_game):
		_game.queue_free()
	_game = null
	add_child(_main_menu)
	_main_menu.connect("game_started", self, "_on_game_started")
	_main_menu.connect("game_exited", self, "_on_game_exited")


func _on_game_exited() -> void:
	GlobalStatus.stop_all_sounds()
	# Go back to CrumbsArcade
	# Transform hardcoded after setting it up in editor
	var transform_in_front_of_machine : Transform = Transform(\
		Vector3(0, 0, -1),\
		Vector3(0, 1, 0),\
		Vector3(1, 0, 0),\
		Vector3(1.104, 0, 6.209)\
	)
	var changes := {
		"Duckson" : {
			"global_transform" : transform_in_front_of_machine
		}
	}
	GlobalStatus.change_scene_to("res://scenes/crumbs_arcade/CrumbsArcade.tscn", true, changes)
