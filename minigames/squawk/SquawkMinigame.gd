extends Control


enum Stages {
	PRESS_REPETEADLY,
	PRESS_ON_INTERVAL,
}

onready var bar := $ProgressBar as ProgressBar
onready var pointer := $Pointer as TextureRect
onready var anim_player := $Pointer/AnimationPlayer as AnimationPlayer
onready var control_hint := $ControlHint as TextureRect
onready var tween := $Tween as Tween
onready var audio_player := $AudioStreamPlayer as AudioStreamPlayer
var remaining_stages := [Stages.PRESS_REPETEADLY, Stages.PRESS_ON_INTERVAL, Stages.PRESS_REPETEADLY,\
		Stages.PRESS_ON_INTERVAL, Stages.PRESS_ON_INTERVAL, Stages.PRESS_REPETEADLY,\
		Stages.PRESS_ON_INTERVAL]
var current_stage = remaining_stages[0]
var current_bar_interval := [0,0] # interval (in px) when the player has to press, initially 0
export(int) var interval_size_in_px := 60
export(PackedScene) var quack_particles_scn = null


var minigame_result = null # bool set when the minigame finishes
var quack_sound_paths := ["res://assets/music/cuac_1.wav", "res://assets/music/cuac_2.wav", "res://assets/music/cuac_3.wav"]
var _last_quack_sound := ""

signal minigame_finished


func _ready() -> void:
	if quack_particles_scn == null:
		printerr("Quack Particles scn is null at SquawkMinigame.tscn")
	_stage_setup(current_stage)
	play_random_quack_sound()


func _input(event: InputEvent) -> void:
	if not event.is_action_pressed("ui_accept") or minigame_result != null: # ignore input if result set
		return
	
	match current_stage:
		Stages.PRESS_REPETEADLY:
			bar.value += 2
		Stages.PRESS_ON_INTERVAL:
			var pointer_val := pointer.rect_position.x + pointer.rect_size.x / 2
			if pointer_val >= current_bar_interval[0] and pointer_val <= current_bar_interval[1]:
				_increment_bar(15)
				var particles = quack_particles_scn.instance()
				add_child(particles)
				particles.position = control_hint.rect_position
			else:
				_increment_bar(-10)
			# Update interval
			var interval_start : int = int(rand_range(0, bar.rect_size.x - interval_size_in_px))
			var interval_end : int = interval_start + interval_size_in_px
			current_bar_interval = [interval_start, interval_end]
			control_hint.rect_position.x = interval_start


func _physics_process(delta: float) -> void:
	if minigame_result != null:
		return # don't run process if result already set
	
	var bar_decrement : int
	match current_stage:
		Stages.PRESS_ON_INTERVAL:
			# Press on interval stage processing
			bar_decrement = 1
		Stages.PRESS_REPETEADLY:
			bar_decrement = 6
	
	bar.value -= bar_decrement * delta
	if bar.value <= 0:
		minigame_result = false
		anim_player.play("show_lose_label")
		yield(anim_player, "animation_finished")
		emit_signal("minigame_finished")
		audio_player.volume_db = -80
	if bar.value >= 99:
		_go_to_next_stage()


func _go_to_next_stage() -> void:
	remaining_stages.pop_front()
	if remaining_stages.size() == 0:
		minigame_result = true
		anim_player.play("show_win_label")
		yield(anim_player, "animation_finished")
		emit_signal("minigame_finished")
		audio_player.volume_db = -80
		return
	
	# Initial setup
	_stage_setup(remaining_stages[0])
	current_stage = remaining_stages[0]


func _stage_setup(stage) -> void:
	match stage:
		Stages.PRESS_REPETEADLY:
			if tween.is_active():
				tween.stop_all()
			bar.value = 25
			pointer.hide()
			anim_player.play("controlhint_arrow")
			control_hint.rect_position.x = (rect_size.x / 2) - (control_hint.rect_size.x / 2)
		
		Stages.PRESS_ON_INTERVAL:
			var interval_start : int = int(rand_range(0, bar.rect_size.x - interval_size_in_px))
			var interval_end : int = interval_start + interval_size_in_px
			current_bar_interval = [interval_start, interval_end]
			# ControlHint position
			control_hint.rect_position.x = interval_start
			anim_player.stop()
			anim_player.play("move")
			pointer.show()
			if tween.is_active():
				tween.stop_all()
			bar.value = 33


func _increment_bar(value : int) -> void:
	tween.interpolate_property(bar, "value", bar.value, bar.value + value, 0.25, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	tween.start()


func play_random_quack_sound() -> void:
	# This method is connected to the finished signal 
	# in audio player, so every time an audio finishes a new
	# one will start with a bit of delay
	
	yield(get_tree().create_timer(1), "timeout")
	
	# Select random path (make sure it doesnt repeat)
	var sound_path = quack_sound_paths[randi() % quack_sound_paths.size()]
	while sound_path == _last_quack_sound:
		sound_path = quack_sound_paths[randi() % quack_sound_paths.size()]
	_last_quack_sound = sound_path
	
	var sound = load(sound_path)
	audio_player.stream = sound
	audio_player.play()
