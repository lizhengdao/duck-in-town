extends HBoxContainer

onready var focus_indicator := $MarginContainer/FocusIndicator as Control
onready var label := $MarginContainer/MarginContainer/Label as Label
export(PoolStringArray) var options := PoolStringArray()
export(int) var selected : int = 0 setget _set_selected


func _ready() -> void:
	label.text = options[selected] if options.size() > 0 else ""


func _input(event: InputEvent) -> void:
	if not has_focus():
		return
	if event.is_action_pressed("ui_left"):
		selected = wrapi(selected - 1, 0, options.size())
		accept_event()
	elif event.is_action_pressed("ui_right"):
		selected = wrapi(selected + 1, 0, options.size())
		accept_event()
	select(selected)


func _on_CustomOptionButton_focus_entered() -> void:
	focus_indicator.self_modulate = Color("79c5da")


func _on_CustomOptionButton_focus_exited() -> void:
		focus_indicator.self_modulate = Color("9cdefa")


func select(idx : int) -> void:
	selected = wrapi(idx, 0, options.size())
	if options.size() > selected:
		$MarginContainer/MarginContainer/Label.text = options[selected]


func _set_selected(idx : int) -> void:
	select(idx)