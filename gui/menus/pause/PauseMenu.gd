extends Control

# Screenshot taken before opening PauseMenu, for saving. Should never be null
var save_data_screenshot : Image = null
export(PackedScene) var save_load_menu_scn = null
export(PackedScene) var main_menu_scn = null
onready var save_button := $MarginContainer/VBoxContainer/Buttons/Save as Button
onready var load_button := $MarginContainer/VBoxContainer/Buttons/Load as Button
onready var exit_button := $MarginContainer/VBoxContainer/Buttons/ExitToMenu as Button
onready var controls_help := $MarginContainer/VBoxContainer/Help as Container
onready var buttons := $MarginContainer/VBoxContainer/Buttons as Container


func _ready() -> void:
	if save_load_menu_scn == null:
		printerr("SaveLoadMenu scn null at PauseMenu.tscn")
	if main_menu_scn == null:
		printerr("MainMenu scn null at PauseMenu.tscn")
	
	if GlobalStatus.is_demo:
		# Disable save handling
		save_button.disabled = true
		load_button.disabled = true
		exit_button.disabled = true


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("pause"):
		if visible:
			hide()
			accept_event()
	elif event.is_action_pressed("ui_cancel"):
		if controls_help.visible:
			_on_Help_Back_pressed()
		else:
			if visible:
				hide()
				accept_event()


func _on_Save_pressed() -> void:
	var savemenu = save_load_menu_scn.instance()
	savemenu.screenshot_data = save_data_screenshot
	GUI.add_child(savemenu)
	hide()
	get_tree().paused = true


func _on_Load_pressed() -> void:
	var savemenu = save_load_menu_scn.instance()
	savemenu.screenshot_data = null
	GUI.add_child(savemenu)
	hide()
	get_tree().paused = true


func _on_ExitToMenu_pressed() -> void:
	GlobalStatus.change_scene_to(main_menu_scn.resource_path)
	hide()
	GUI.action_list.actions = []
	get_tree().paused = false


func _on_PauseMenu_visibility_changed() -> void:
	if visible:
		get_tree().paused = true
		_on_Help_Back_pressed()
	else:
		get_tree().paused = false


func _on_Help_pressed() -> void:
	$MarginContainer/VBoxContainer/Help/Buttons/Back.grab_focus()
	buttons.hide()
	controls_help.show()


func _on_Help_Back_pressed() -> void:
	save_button.grab_focus()
	controls_help.hide()
	buttons.show()
