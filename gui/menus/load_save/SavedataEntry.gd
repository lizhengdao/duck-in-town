extends MarginContainer

var savedata_slot : int = -1 setget _set_savedata_slot
var screenshot_data : Image = null # If not null, the menu is in save mode instead of load mode
onready var time_saved_label := $MarginContainer/HBoxContainer/Data/TimeSaved as Label
onready var time_played_label := $MarginContainer/HBoxContainer/Data/TimePlayed as Label
onready var slot_number_label := $MarginContainer/HBoxContainer/SlotNumber as Label
onready var empty_label := $MarginContainer/HBoxContainer/Data/Empty as Label
onready var button := $Button as ToolButton
onready var screenshot := $MarginContainer/HBoxContainer/Screenshot as TextureRect
onready var background := $Background as ColorRect

signal slot_to_load_selected

func _set_savedata_slot(slot : int) -> void:
	savedata_slot = slot
	slot_number_label.text = str(slot)
	
	var file_content : Dictionary = GlobalStatus.get_savedata_content(slot)
	
	if file_content.empty():
		time_saved_label.hide()
		time_played_label.hide()
		empty_label.show()
		button.disabled = screenshot_data == null # if not null, we are in save mode and this slot can be used
		return
	
	# Load screenshot for this savedata
	var img_screenshot := Image.new()
	var screenshot_tex := ImageTexture.new()
	if img_screenshot.load("user://saves/" + str(savedata_slot) + ".png") == OK:
		screenshot_tex.create_from_image(img_screenshot)
		screenshot.texture = screenshot_tex
	screenshot.self_modulate.a = 1.0
	
	var date_saved := OS.get_datetime_from_unix_time(int(file_content["date_saved"]) + (OS.get_time_zone_info().bias * 60))
	var unix_time_played : float = float(file_content["time_played"])
	var hours := unix_time_played / 3600
	var minutes_remainder := hours - floor(hours)
	hours = int(floor(hours))
	var minutes := int(range_lerp(minutes_remainder, 0.0, 1.0, 0, 60))
	
	time_played_label.show()
	time_saved_label.show()
	empty_label.hide()
	time_played_label.text = tr("PLAY_TIME_LABEL") + ": " + str(hours).pad_zeros(2) + ":" + str(minutes).pad_zeros(2)
	time_saved_label.text = str(date_saved["day"]) + "/" + str(date_saved["month"]) + "/" + \
			str(date_saved["year"]) + " - " + str(date_saved["hour"]).pad_zeros(2) + ":" + str(date_saved["minute"]).pad_zeros(2)


func _on_Button_pressed() -> void:
	if screenshot_data == null:
		# Menu in Load mode
		GlobalStatus.load_data(savedata_slot)
		# Disable button to avoid crash by repeteadly pressing the button while the scene is loading
		$Button.disabled = true
		emit_signal("slot_to_load_selected")
	else:
		# Menu in save mode
		GlobalStatus.save_data(savedata_slot, screenshot_data)
		# Refresh entry
		_set_savedata_slot(savedata_slot)


func _on_SavedataEntry_focus_entered() -> void:
	button.grab_focus()


func _on_Button_focus_entered() -> void:
	background.self_modulate = Color("8f557a87")


func _on_Button_focus_exited() -> void:
	background.self_modulate = Color("#8f85c0d5")
