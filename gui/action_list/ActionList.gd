extends MarginContainer

var actions := [] setget _set_actions
onready var custom_scroll_container := $HBoxContainer/CustomScrollContainer as ScrollContainer
onready var entries := $HBoxContainer/CustomScrollContainer/Entries as VBoxContainer
onready var up_arrow := $HBoxContainer/Arrows/UpContainer/Up as TextureRect
onready var down_arrow := $HBoxContainer/Arrows/DownContainer/Down as TextureRect
export(PackedScene) var action_list_entry_scn = null


func _ready() -> void:
	if action_list_entry_scn == null:
		printerr("The action list entry scn is null.")
		return


func _physics_process(delta: float) -> void:
	var focus_owner : Control = get_focus_owner()
	if entries.get_child_count() > 0 and visible:
		if not focus_owner in entries.get_children():
			entries.get_child(0).grab_focus()

	# Arrows handling
	var entries_number : int = entries.get_child_count()
	var last_entry_idx : int = entries_number - 1
	if entries_number > 2 and focus_owner in entries.get_children():
		# With more than 2 entries, scrollbar appearing is guaranteed
		for i in range(entries_number):
			# Hide or show arrows depending on the entry focused and 
			# its position
			if focus_owner == entries.get_child(i):
				match i:
					0:
						up_arrow.hide()
						down_arrow.show()
					last_entry_idx:
						up_arrow.show()
						down_arrow.hide()
					_:
						up_arrow.show()
						down_arrow.show()
	else:
		up_arrow.hide()
		down_arrow.hide()


func _set_actions(acts : Array) -> void:
	actions = acts
	for c in entries.get_children():
		c.queue_free()
	for action in actions:
		var entry : Control = action_list_entry_scn.instance()
		entries.add_child(entry)
		entry.connect("focus_entered", custom_scroll_container, "_on_child_gained_focus", [entry])
		entry.set_action(action)