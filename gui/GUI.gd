extends Control

signal dialog_started
signal dialog_finished
onready var action_list : MarginContainer = $MarginContainer/ActionList as MarginContainer
onready var margin_container : MarginContainer = $MarginContainer as MarginContainer
onready var tween : Tween = $Tween as Tween
onready var load_spinner := $CanvasLayer/LoadSpinner as TextureRect
onready var pause_menu := $CanvasLayer2/PauseMenu as Control

export(PackedScene) var dialog_box_scn = null
export(PackedScene) var item_selector_scn = null
export(PackedScene) var item_get_notif_scn = null

func _ready() -> void:
	if dialog_box_scn == null:
		printerr("Dialog box scn is null.")
	if item_selector_scn == null:
		printerr("Item selector scn is null.")
	if item_get_notif_scn == null:
		printerr("Item get scn is null.")


func set_actions(actions : Array) -> void:
	action_list.actions = actions


func start_dialog(dialog_id : String, characters_in_dialog : Array) -> void:
	emit_signal("dialog_started")
	var box = dialog_box_scn.instance()
	box.name = "DialogBox"
	
	var dialog_path : String
	var current_locale : String = TranslationServer.get_locale()
	if current_locale.begins_with("es"):
		dialog_path = "res://assets/dialog/" + dialog_id + "_es.yarn.txt"
	else:
		dialog_path = "res://assets/dialog/" + dialog_id + ".yarn.txt"
	
	# Connect signals
	if GlobalStatus.player != null and is_instance_valid(GlobalStatus.player):
		box.connect("current_character_name_changed", GlobalStatus.player, "_current_dialog_char_name_changed")
		box.connect("dialog_finished", GlobalStatus.player, "_on_dialog_finished")
		if characters_in_dialog.size() > 0:
			# rotate Duckson towards the object
			characters_in_dialog[0].rotate_player_towards_object()
	box.connect("dialog_finished", self, "_on_dialog_finished")
	for character in characters_in_dialog:
		box.connect("dialog_finished", character, "_on_dialog_finished")
		box.connect("current_character_name_changed", character, "_current_dialog_char_name_changed")
	
	action_list.hide()
	# Show cinematic borders before showing the dialog box
	yield(_show_cinematic_borders(), "completed")
	add_child(box)
	box.start(dialog_path)


func _on_dialog_finished() -> void:
	yield(_hide_cinematic_borders(), "completed")
	action_list.show()
	emit_signal("dialog_finished")


func select_item() -> Item:
	var menu = item_selector_scn.instance()
	action_list.hide()
	self.add_child(menu)
	yield(menu, "item_selected")
	var item : Item = menu.selected_item
	menu.queue_free()
	action_list.show()
	return item


func show_item_obtained_notification(item_id : String) -> void:
	var n = item_get_notif_scn.instance()
	n.item_id = item_id
	add_child(n)


func _show_cinematic_borders() -> void:
	$CinematicBorders.show()
#warning-ignore:return_value_discarded
	tween.interpolate_property($CinematicBorders/Top, "rect_scale:y", 0, 1, 0.25, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.interpolate_property($CinematicBorders/Bottom, "rect_scale:y", 0, 1, 0.25, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
#warning-ignore:return_value_discarded
	tween.start()
	yield(tween, "tween_completed")


func _hide_cinematic_borders() -> void:
#warning-ignore:return_value_discarded
	tween.interpolate_property($CinematicBorders/Top, "rect_scale:y", 1, 0, 0.25, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.interpolate_property($CinematicBorders/Bottom, "rect_scale:y", 1, 0, 0.25, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
#warning-ignore:return_value_discarded
	tween.start()
	yield(tween, "tween_completed")
	$CinematicBorders.hide()


func show_load_spinner() -> void:
	load_spinner.show()
	tween.interpolate_property(load_spinner, "self_modulate:a", 0.0, 1.0, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()


func hide_load_spinner() -> void:
	tween.interpolate_property(load_spinner, "self_modulate:a", 1.0, 0.0, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	yield(tween, "tween_completed")
	load_spinner.hide()
