tool
extends TextureRect
class_name ControlHint


export(Texture) var mouse_keyboard = null setget _set_mouse_keyboard
export(Texture) var pc_controller = null setget _set_pc_controller


func _ready() -> void:
	GlobalStatus.connect("started_using_mouse_and_keyboard", self, "_on_started_using_mouse_and_keyboard")
	GlobalStatus.connect("started_using_gamepad", self, "_on_started_using_gamepad")
	if GlobalStatus.using_gamepad():
		_on_started_using_gamepad()
	elif GlobalStatus.using_mouse_and_keyboard():
		_on_started_using_mouse_and_keyboard()


func _set_mouse_keyboard(tex : Texture) -> void:
	mouse_keyboard = tex
	texture = tex


func _set_pc_controller(tex : Texture) -> void:
	pc_controller = tex
	# By default we show the mouse_keyboard texture
	# in editor, so we will only set controller texture
	# if mouse_kb hasn't been set
	if mouse_keyboard == null:
		texture = pc_controller


func _on_started_using_mouse_and_keyboard() -> void:
	if mouse_keyboard == null:
		self.hide()
	else:
		self.show()
		texture = mouse_keyboard


func _on_started_using_gamepad() -> void:
	if pc_controller == null:
		self.hide()
	else:
		self.show()
		texture = pc_controller
