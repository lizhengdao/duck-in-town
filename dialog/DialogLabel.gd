extends RichTextLabel

export(int, "Very Fast", "Fast", "Normal", "Slow", "Very Slow") var text_speed = 2

enum {WAIT_STATE, PRINT_STATE}
var state := WAIT_STATE
var _delay : float = 0

signal printing_ended

func _ready() -> void:
	bbcode_enabled = true
	percent_visible = 0


func _physics_process(delta : float) -> void:
	if percent_visible >= 1.0 or state == WAIT_STATE:
		if state != WAIT_STATE:
			state = WAIT_STATE
			emit_signal("printing_ended")
		return
	
	_delay += delta * 50
	if _delay > text_speed:
		_delay = 0
		if get_total_character_count() > 0:
			percent_visible += (1.0 / get_total_character_count())


# Show single string
func show_animated_text(data : String):
	percent_visible = 0
	bbcode_text = data
	state = PRINT_STATE


# Clear the content
func reset() -> void:
	bbcode_text = ""
	state = WAIT_STATE


func end_text_animation() -> void:
	percent_visible = 1


# Resume the activity of the label
func play() -> void:
	if percent_visible < 1.0:
		state = PRINT_STATE


# Pause the activity of the label
func stop() -> void:
	state = WAIT_STATE
