extends Object

# Map of available (and loaded) yarn files. yarn_id:(yarn_node_id:YarnNode)
var yarns := {}

func import(path : String) -> bool:
	
	var f = File.new()
	var err = f.open(path, File.READ)
	
	if err != OK:
		printerr("An error happened loading Yarn file at ", path)
		return false
	
	var yarn_nodes := {}
	# The file starts with a header
	var is_header := true
	var current_node := YarnNode.new()
	
	while not f.eof_reached():
		var line = f.get_line()
		
		if is_header:
			if line == "---":
				# header finished
				is_header = false
			else:
				# parse header
				var split = line.split(": ")
				if split[0] == "title":
					current_node.id = split[1]
				elif split[0] == "tags":
					var tags : Array = split[1].split(" ")
					current_node.tags = PoolStringArray(tags)
		elif line == "===":
			# next line will be a header, current node is completed
			is_header = true
			yarn_nodes[current_node.id] = current_node
			current_node = YarnNode.new()
		else:
			if line.begins_with("#"):
				# this line is character name
				current_node.character_name = line.replace("#", "")
				continue
			# parsing lines
			var new_line := YarnLine.new()
			if line.begins_with("[["):
				# this line is a choice/redirection
				line = line.replace("[[", "")
				line = line.replace("]] ", "") # sometimes yarn introduces space at the end of line
				line = line.replace("]]", "")
				if line.findn("|") != -1:
					# this line is a choice
					var choice_split = line.split("|")
					new_line.type = YarnLine.Type.CHOICE
					new_line.text = choice_split[0]
					new_line.next_node_id = choice_split[1]
				else:
					# this line is a redirection
					new_line.type = YarnLine.Type.REDIRECT
					new_line.next_node_id = line
			elif line.begins_with("<<"):
				# this line is a command
				new_line.type = YarnLine.Type.COMMAND
				new_line.text = line
			else:
				# this line is a text line
				new_line.type = YarnLine.Type.TEXT
				new_line.text = line
			# Append new line to current node
			current_node.lines.append(new_line)
	
	yarns[path] = yarn_nodes
	return true


func get_start_node(yarn_id : String) -> YarnNode:
	if not yarns.has(yarn_id) and not import(yarn_id):
			return null
	
	var ret : YarnNode = null
	for n in yarns[yarn_id].keys():
		if n == "Start":
			ret = yarns[yarn_id][n]
			break
	
	return ret


func get_node_by_id(yarn_id : String, node_id : String) -> YarnNode:
	if yarns.has(yarn_id) and yarns[yarn_id].has(node_id):
		return yarns[yarn_id][node_id]
	else:
		return null