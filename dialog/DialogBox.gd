extends VBoxContainer

var yarn_id : String
var current_yarn_node : YarnNode = null
var line_idx := 0
var current_char_name := "" # name of character/item currently speaking
export(PackedScene) var dialog_option_scn = null
onready var option_list := $Box/MarginContainer/HBoxContainer/CustomScrollContainer/OptionList as VBoxContainer
onready var box_container := $Box as MarginContainer
onready var char_name_label := $MarginContainer/CharacterName as Label
onready var dialog_line_container := $Box/DialogLine as MarginContainer
onready var option_list_container := $Box/MarginContainer/HBoxContainer/CustomScrollContainer as ScrollContainer
onready var dialog_line_label := $Box/DialogLine/DialogLabel as RichTextLabel
onready var dialog_line_ended_indicator := $Box/DialogLine/Indicator as TextureRect
onready var background := $Box/Background/BackgroundTex as TextureRect # This has a neutral Control as a parent to help with scaling (containers don't allow to do it smoothly)
onready var up_arrow := $Box/MarginContainer/HBoxContainer/Arrows/UpContainer/Up as TextureRect
onready var down_arrow := $Box/MarginContainer/HBoxContainer/Arrows/DownContainer/Down as TextureRect
onready var tween := $Tween as Tween
onready var accept_sound := $AcceptSound as AudioStreamPlayer

signal dialog_finished
signal current_character_name_changed

enum States {
	WAITING,
	CHOOSING,
	PRINTING,
}

var current_state = States.PRINTING

func start(_yarn_id : String) -> void:
	current_yarn_node = DialogDB.get_start_node(_yarn_id)
	if current_yarn_node == null:
		printerr("No Start node found in Yarn file at: ", _yarn_id)
		return
	
	yield(_show_background(), "completed")
	yarn_id = _yarn_id
	_go_to_next_line()


func _ready() -> void:
	dialog_line_label.connect("printing_ended", self, "_on_printing_ended")
	if dialog_option_scn == null:
		printerr("Dialog option scn is null.")
	
	up_arrow.hide()
	down_arrow.hide()


func _physics_process(delta : float):
	if current_state == States.WAITING:
		_show_indicator()
	else:
		_hide_indicator()
	
	# Arrow handling
	if current_state == States.CHOOSING:
		var focus_owner : Control = get_focus_owner()
		var entries_number : int = option_list.get_child_count()
		var last_entry_idx : int = entries_number - 1
		if focus_owner in option_list.get_children():
			for i in range(entries_number):
				# Hide or show arrows depending on the entry focused and 
				# its position
				if option_list.get_child(i) == focus_owner:
					match i:
						0:
							up_arrow.hide()
							down_arrow.show()
						last_entry_idx:
							up_arrow.show()
							down_arrow.hide()
						_:
							up_arrow.show()
							down_arrow.show()
		else:
			up_arrow.hide()
			down_arrow.hide()
	else:
		up_arrow.hide()
		down_arrow.hide()


func _input(event : InputEvent) -> void:
	if event.is_action_released("ui_accept"):
		match current_state:
			States.PRINTING:
				dialog_line_label.end_text_animation()
				current_state = States.WAITING
			States.WAITING:
				_go_to_next_line()
				accept_sound.play()


func _end_dialog() -> void:
	yield(_hide_background(), "completed")
	emit_signal("dialog_finished")
	queue_free()


func _go_to_next_line() -> void:
	current_state = States.PRINTING
	
	if line_idx >= current_yarn_node.lines.size():
		# No more lines available, dialog is finished
		_end_dialog()
		return
	
	var line : YarnLine = current_yarn_node.lines[line_idx]
	
	match line.type:
		YarnLine.Type.TEXT:
			dialog_line_container.show()
			option_list_container.hide()
			char_name_label.text = current_yarn_node.character_name
			if char_name_label.text != current_char_name:
				_change_current_char_name(char_name_label.text)
			dialog_line_label.show_animated_text(line.text)
			line_idx += 1
		YarnLine.Type.CHOICE:
			current_state = States.CHOOSING
			dialog_line_container.hide()
			_change_current_char_name("")
			option_list_container.show()
			_fill_options()
		YarnLine.Type.REDIRECT:
			current_yarn_node = DialogDB.get_node_by_id(yarn_id, line.next_node_id)
			line_idx = 0
			dialog_line_container.hide()
			_change_current_char_name("")
			option_list_container.hide()
			_go_to_next_line()
		YarnLine.Type.COMMAND:
			# Command is like:
			# <<command_id arg1 arg2 arg3 arg_n>>
			var command_line = line.text.replace("<<", "")
			command_line = command_line.replace(">> ", "")
			command_line = command_line.replace(">>", "")
			var command_split : Array = command_line.split(" ")
			var command_id = command_split[0]
			var args = []
			for i in range(1, command_split.size()):
				args.append(command_split[i])
			_run_command(command_id, args)
			line_idx += 1
			_go_to_next_line()


func _run_command(command_id : String, args : Array) -> void:
	match command_id:
		"end_dialog":
			_end_dialog()
		"activate_flag":
			GlobalStatus.add_story_flag(args[0])
		"add_item":
			GlobalStatus.add_item(args[0])
		"remove_item":
			GlobalStatus.remove_item(args[0])
		"go_to_from":
			if args[0] == "forest" and args[1] == "town":
				# To forest from town
				var transform_in_front_of_door : Transform = Transform(\
					Vector3(0, 0, 1),\
					Vector3(0, 1, 0),\
					Vector3(-1, 0, 0),\
					Vector3(14.75, 0, 0)\
				)
				var changes := {
					"Duckson" : {
						"global_transform" : transform_in_front_of_door
					}
				}
				GlobalStatus.change_scene_to("res://scenes/forest/Forest.tscn", true, changes)
			elif args[0] == "town" and args[1] == "forest":
				# To town from forest
				GlobalStatus.change_scene_to("res://scenes/town/Town.tscn")


func _fill_options() -> void:
	for l in current_yarn_node.lines:
		if l.type == YarnLine.Type.CHOICE:
			var entry : Control = dialog_option_scn.instance()
			option_list.add_child(entry)
			entry.text = l.text
#warning-ignore:return_value_discarded
			entry.connect("pressed", self, "_option_pressed", [l.next_node_id])
			entry.connect("focus_entered", option_list_container, "_on_child_gained_focus", [entry])
	
	var options_number = option_list.get_child_count()
	for i in range(options_number):
		if options_number >= 2:
			if i == 0:
				option_list.get_child(0).focus_neighbour_bottom = option_list.get_child(1).get_path()
			elif i == options_number - 1:
				option_list.get_child(i).focus_neighbour_top = option_list.get_child(i-1).get_path()
			else:
				option_list.get_child(i).focus_neighbour_bottom = option_list.get_child(i+1).get_path()
				option_list.get_child(i).focus_neighbour_top = option_list.get_child(i-1).get_path()
	
	option_list.get_child(0).grab_focus()


func _option_pressed(next_node_id : String) -> void:
	# delete buttons
	for c in option_list.get_children():
		c.queue_free()
	
	option_list_container.hide()
	dialog_line_container.hide()
	_change_current_char_name("")
	
	# set new yarn node
	current_yarn_node = DialogDB.get_node_by_id(yarn_id, next_node_id)
	line_idx = 0
	_go_to_next_line()


func _change_current_char_name(new_name : String) -> void:
	current_char_name = new_name
	char_name_label.text = current_char_name
	emit_signal("current_character_name_changed", current_char_name)
	
	# Change color on name label
	var color := Color()
	match current_char_name:
		"Darling":
			color = Color("f4afff")
		"Dogge":
			color = Color("9de1ff")
		"Duck":
			color = Color("ffffff")
		"Ducker III", "Ducker IV":
			color = Color("ff6767")
		"Durkson":
			color = Color("5d5d5d")
		"Fulcrum":
			color = Color("9751ff")
		"Goldie":
			color = Color("ffad00")
		"Greenier":
			color = Color("72de62")
		"Gregor":
			color = Color("aba56e")
		"HD":
			color = Color("578fdf")
		"Mike":
			color = Color("578fdf")
		"Arnold":
			color = Color("25c633")
		"Lilly":
			color = Color("ca8de3")
		"Lucky":
			color = Color("ffffff")
		"Pato":
			color = Color("b78b5c")
		"Duckson":
			color = Color("e5df81")
		_:
			color = Color("578fdf") # default
	char_name_label.set("custom_colors/font_color", color)


func _show_indicator():
	if not dialog_line_ended_indicator.get_node("AnimationPlayer").is_playing():
		dialog_line_ended_indicator.get_node("AnimationPlayer").play("blink_indicator")
	dialog_line_ended_indicator.modulate.a = 0.8


func _hide_indicator():
	dialog_line_ended_indicator.modulate.a = 0


func _on_printing_ended() -> void:
	current_state = States.WAITING


func _show_background() -> void:
	char_name_label.text = ""
	dialog_line_label.text = ""
#warning-ignore:return_value_discarded
	tween.interpolate_property(background, "rect_scale:x", 0, 1, 0.3, Tween.TRANS_QUAD, Tween.EASE_IN)
	background.rect_scale.x = 0
	box_container.show()
#warning-ignore:return_value_discarded
	tween.start()
	yield(tween, "tween_completed")


func _hide_background() -> void:
	char_name_label.text = ""
	dialog_line_label.text = ""
#warning-ignore:return_value_discarded
	tween.interpolate_property(background, "rect_scale:x", 1, 0, 0.3, Tween.TRANS_QUAD, Tween.EASE_IN)
#warning-ignore:return_value_discarded
	tween.start()
	yield(tween, "tween_completed")
	box_container.hide()
