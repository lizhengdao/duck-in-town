extends Object
class_name Item

var id : String
var name : String setget ,_get_name
var description : String setget ,_get_description
var icon_path : String # path to icon asset

func _get_name() -> String:
	return tr(name)

func _get_description() -> String:
	return tr(description)
