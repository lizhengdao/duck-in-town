shader_type spatial;
render_mode unshaded, cull_disabled;

uniform sampler2D fire_tex_1 : hint_albedo;
uniform sampler2D fire_tex_2 : hint_albedo;
uniform sampler2D fire_mask : hint_albedo;
uniform vec4 inner_color : hint_color;
uniform vec4 outer_color : hint_color;

void vertex() {
	// Y-Billboard
	MODELVIEW_MATRIX = INV_CAMERA_MATRIX * mat4(CAMERA_MATRIX[0],WORLD_MATRIX[1],vec4(normalize(cross(CAMERA_MATRIX[0].xyz,WORLD_MATRIX[1].xyz)), 0.0),WORLD_MATRIX[3]);
	MODELVIEW_MATRIX = MODELVIEW_MATRIX * mat4(vec4(1.0, 0.0, 0.0, 0.0),vec4(0.0, 1.0/length(WORLD_MATRIX[1].xyz), 0.0, 0.0), vec4(0.0, 0.0, 1.0, 0.0),vec4(0.0, 0.0, 0.0 ,1.0));
}

void fragment() {
	vec3 noise_1 = texture(fire_tex_1, vec2(UV.x, UV.y + TIME * 0.7)).rgb;
	vec3 noise_2 = texture(fire_tex_2, vec2(UV.x, UV.y + TIME * 1.2)).rgb;
	vec3 mask = texture(fire_mask, UV).rgb;
	
	vec3 col = (noise_1 + noise_2) * mask;
	ALBEDO = vec3(step(0.015, col.r));
	ALBEDO *= mix(outer_color.rgb, inner_color.rgb, step(0.15, col.r));
	ALPHA = step(0.015, col.r);
}