extends Node

#warning-ignore:unused_class_variable
var player : KinematicBody
var owned_items := [] # item_ids
# warning-ignore:unused_class_variable
onready var tween := $Tween as Tween
onready var fade_anim := $Fade/Fade/AnimationPlayer as AnimationPlayer

var current_story_flags := []
var papeo_island_high_score := 0

# Vars for using GlobalStatus as a scene switcher
var current_scene : Node = null
var load_thread := Thread.new()

# Time tracking
var time_loaded = null # unix time when the current save was loaded
var slot_loaded_from = null

# Controller tracking
var current_input_type := "keyboard"
signal started_using_mouse_and_keyboard
signal started_using_gamepad

var is_demo := false


func _ready() -> void:
	randomize()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	current_scene = get_tree().root.get_child(get_tree().root.get_child_count() - 1)
	$BetaLabel/Label.visible = is_demo


func _input(event):
	if event is InputEventMouseMotion or event is InputEventMouseButton or event is InputEventKey:
		if current_input_type != "keyboard":
			emit_signal("started_using_mouse_and_keyboard")
		current_input_type = "keyboard"
	elif event is InputEventJoypadButton or event is InputEventJoypadMotion:
		if current_input_type != "gamepad":
			emit_signal("started_using_gamepad")
		current_input_type = "gamepad"
	
	if event.is_action_pressed("reset_demo") and is_demo:
		_reset_game()


func using_mouse_and_keyboard() -> bool:
	return current_input_type == "keyboard"


func using_gamepad() -> bool:
	return current_input_type == "gamepad"


func _exit_tree() -> void:
	# needed for portability
	load_thread.wait_to_finish()


func play_stream(stream_path : String, loop : bool = false) -> void:
	# loop_mode from AudioStreamSample.LOOP_MODE_*
	$AudioStreamPlayer.stream = load(stream_path)
	$AudioStreamPlayer.stream.loop = loop
	$AudioStreamPlayer.play()


func play_scene_music(stream_path : String, volume : float = 0) -> void:
	if not $AudioStreamPlayer.playing or $AudioStreamPlayer.stream.resource_path != stream_path:
		if $AudioStreamPlayer.playing: # fade out previous music
			tween.interpolate_property($AudioStreamPlayer, "volume_db", $AudioStreamPlayer.volume_db, -100, 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
			tween.start()
			yield(tween, "tween_completed")
		$AudioStreamPlayer.stream = load(stream_path)
		$AudioStreamPlayer.stream.loop = true
		$AudioStreamPlayer.volume_db = -100
		$AudioStreamPlayer.play()
		# Fade in
		tween.interpolate_property($AudioStreamPlayer, "volume_db", $AudioStreamPlayer.volume_db, volume, 0.3, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		tween.start()
	
	if $AudioStreamPlayer.stream.resource_path == stream_path and $AudioStreamPlayer.volume_db != volume:
		# Same audio as playing requested, but with different volume
		$AudioStreamPlayer.volume_db = volume


func stop_all_sounds() -> void:
	if $AudioStreamPlayer.playing:
		$AudioStreamPlayer.stop()
	if $SoundPlayer.playing:
		$SoundPlayer.stop()


func play_sound(path : String, db : float = 0) -> void:
	$SoundPlayer.volume_db = db
	$SoundPlayer.stream = load(path)
	$SoundPlayer.play()


func add_story_flag(flag : String) -> void:
	var repeteable_flags := ["barrel_given"] # flags that can be added more than once
	if not flag in repeteable_flags and current_story_flags.has(flag):
		return
	current_story_flags.append(flag)


func remove_story_flag(flag : String) -> void:
	if current_story_flags.has(flag):
		current_story_flags.remove(current_story_flags.find(flag))


func story_flags_unlocked(flags : Array) -> bool:
	for flag in flags:
		if not current_story_flags.has(flag):
			return false
	return true


func get_savedata_content(slot_idx : int) -> Dictionary:
	var file := File.new()
	var err := file.open("user://saves/" + str(slot_idx) + ".sav", File.READ)
	
	if err != OK or not file.file_exists("user://saves/" + str(slot_idx) + ".sav"):
		file.close()
		return {}
	
	var data : Dictionary = parse_json(file.get_line())
	file.close()
	return data


func save_data(slot_idx : int, img_data : Image = null) -> void:
	var sys_secs := OS.get_system_time_secs()
	var file_content = get_savedata_content(slot_loaded_from) if slot_loaded_from != null else {}
	
	# preserve date created in case of overwriting the file
	var date_created := sys_secs if file_content.empty() else file_content["date_created"]
	
	var time_played
	if file_content.empty():
		time_played = OS.get_system_time_secs() - time_loaded
	else:
		time_played = file_content["time_played"] + OS.get_system_time_secs() - time_loaded
	# update slot loaded from to slot saved
	slot_loaded_from = slot_idx
	# Update last saved date
	time_loaded = OS.get_system_time_secs()
	
	var dir := Directory.new()
	if not dir.dir_exists("user://saves"):
		dir.make_dir("user://saves")
	
	var image := img_data if img_data != null else get_viewport().get_texture().get_data()
	if img_data == null:
		# If provided as an argument, the img must be already flipped
		image.flip_y()
	image.save_png("user://saves/" + str(slot_idx) + ".png")
	
	var data = {
		"current_story_flags" : current_story_flags,
		"current_scene_name" : current_scene.name,
		"owned_items" : owned_items,
		"player_global_transform" : player.global_transform,
		# date when this file is created, only modify if new save
		"date_created" : date_created,
		# date when this saving happens
		"date_saved" : sys_secs, 
		# time played on this save
		"time_played" : time_played,
		"papeo_island_high_score" : papeo_island_high_score,
	}
	
	var file := File.new()
	var err := file.open("user://saves/" + str(slot_idx) + ".sav", File.WRITE)
	
	if err != OK:
		printerr("There was an error saving the data.")
		return
	
	file.store_line(to_json(data))
	file.close()
	play_sound("res://assets/music/save_game.wav", -15)


func load_data(slot_idx : int) -> void:
	var data := get_savedata_content(slot_idx)
	if data.empty():
		printerr("Data at slot " + str(slot_idx) + " couldn't be loaded. Does it exist?")
		return
	
	var scene_to_load := ""
	time_loaded = OS.get_system_time_secs()
	slot_loaded_from = slot_idx
	
	match data["current_scene_name"]:
		"Forest":
			scene_to_load = "res://scenes/forest/Forest.tscn"
		"Town":
			scene_to_load = "res://scenes/town/Town.tscn"
		"Crumbs":
			scene_to_load = "res://scenes/crumbs/Crumbs.tscn"
		"CrumbsArcade":
			scene_to_load = "res://scenes/crumbs_arcade/CrumbsArcade.tscn"
		"Guild":
			scene_to_load = "res://scenes/guild/Guild.tscn"
		_:
			printerr("Scene with name: ", data["current_scene_name"], " not registered.")
	
	current_story_flags = data["current_story_flags"]
	papeo_island_high_score = data["papeo_island_high_score"]
	owned_items = data["owned_items"]
	var player_xform := _parse_transform_string(data["player_global_transform"])
	
	# Dict containing node changes, should be applied
	# after the scene loads. As a key, we use the NodePath,
	# then we have a dict with property_name:property_value as pairs
	var changes := {
		"Duckson": {
			"global_transform" : player_xform,
		}
	}
	
	change_scene_to(scene_to_load, true, changes)


func change_scene_to(path : String, fade : bool = true, scene_changes : Dictionary = {}) -> void:
	# Call deferred to be able to free the current scene before
	# adding the new one to the tree
	if time_loaded == null:
		time_loaded = OS.get_system_time_secs()
	call_deferred("_deferred_change_scene_to", path, fade, scene_changes)


func _deferred_change_scene_to(path : String, fade : bool = true, scene_changes : Dictionary = {}) -> void:
	GUI.action_list.hide()
	GUI.set_actions([])
	if fade:
		fade_anim.play("fade_out")
		yield(fade_anim, "animation_finished")
		GUI.show_load_spinner()
	else:
		GUI.show_load_spinner()
	
	# Background loading and actual scene switching
	# done in another thread
	if load_thread.is_active():
		load_thread.wait_to_finish()
		load_thread = Thread.new()
	#load_thread.start(self, "_background_load_scene_switch", [path, fade, scene_changes])
	# the game sometimes bugs when background loading... 
	# disabling it til I know how to fix it
	_background_load_scene_switch([path, fade, scene_changes])


func _background_load_scene_switch(args : Array) -> void:
	# Unpack args
	var scene_path : String = args[0]
	var fade : bool = args[1]
	var scene_changes : Dictionary = args[2]
	
	#var ril := ResourceLoader.load_interactive(scene_path)
	#ril.wait()
	
	#var scene = ril.get_resource().instance()
	var scene = load(scene_path).instance()
	if current_scene != null and is_instance_valid(current_scene):
		current_scene.call_deferred("free")
	current_scene = scene
	
	# Apply scene changes before adding the scene to the tree
	# scene_changes dict must be like this:
	# {
	#	"Node_path" : {
	#		"first_property" : first_property_value,
	#		"second_property": second_property_value,
	#	},
	#	"Second_node" : {
	#		"property" : value,
	#	}
	# }
	
	for node_path in scene_changes.keys():
		if not current_scene.has_node(node_path):
			printerr("The node at: ", node_path, " can't be found.")
			continue
		var props : Dictionary = scene_changes[node_path]
		for prop_name in props.keys():
			var prop_value = props[prop_name]
			current_scene.get_node(node_path).set(prop_name, prop_value)
	

	get_tree().root.call_deferred("add_child", current_scene)
	get_tree().call_deferred("set", "current_scene", current_scene)
	
	if fade:
		fade_anim.play("fade_in")
		GUI.hide_load_spinner()
		yield(fade_anim, "animation_finished")
	else:
		GUI.hide_load_spinner()
	GUI.action_list.show()


func _parse_transform_string(s : String) -> Transform:
	# Transforms are serialized like this:
	# basis.x, basis.y, basis.z - origin.x, origin.y, origin.z
	# note that basis.x, y and z are actually three vectors, one for 
	# each column on the matrix. However, origin.xyz is a Vec3
	var s_split := s.split(" - ")
	var basis_str_split = s_split[0].split(",")
	var origin_str_split = s_split[1].split(",")
	
	var ret := Transform()
	
	# Parse basis
	ret.basis.x = Vector3(float(basis_str_split[0]), float(basis_str_split[1]), float(basis_str_split[2]))
	ret.basis.y = Vector3(float(basis_str_split[3]), float(basis_str_split[4]), float(basis_str_split[5]))
	ret.basis.z = Vector3(float(basis_str_split[6]), float(basis_str_split[7]), float(basis_str_split[8]))
	
	# Parse origin
	ret.origin = Vector3(float(origin_str_split[0]), float(origin_str_split[1]), float(origin_str_split[2]))
	
	return ret


func add_item(item_id : String) -> void:
	if ItemDB.items.keys().has(item_id):
		owned_items.append(item_id)
		GUI.show_item_obtained_notification(item_id)


func remove_item(item_id : String) -> void:
	for idx in range(owned_items.size()):
		if owned_items[idx] == item_id:
			owned_items.remove(idx)
			return


# Reset the game. Useful for demo resetting
func _reset_game() -> void:
	self.owned_items = []
	self.current_story_flags = []
	change_scene_to("res://scenes/forest/Forest.tscn")


func _on_DemoTimer_timeout() -> void:
	# This timer gets started in Town.tscn
	if player.is_on_dialog:
		yield(GUI, "dialog_finished")
	change_scene_to("res://gui/DemoEndedScreen.tscn")
